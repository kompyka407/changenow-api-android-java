package io.changenow.api.model.responce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by samosudovd on 07/06/2018.
 */

public class CurrencyItem {

        @Expose
        @SerializedName("id")
        private String id;
        @Expose
        @SerializedName("ticker")
        private String ticker;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("hasExternalId")
        private boolean hasExternalId;
        @Expose
        @SerializedName("isFiat")
        private boolean isFiat;
        @Expose
        @SerializedName("isAvailable")
        private boolean isAvailable;

        public String getId() {
                return id;
        }

        public void setId(String id) {
                this.id = id;
        }

        public String getTicker() {
                return ticker;
        }

        public void setTicker(String ticker) {
                this.ticker = ticker;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public String getImage() {
                return image;
        }

        public void setImage(String image) {
                this.image = image;
        }

        public boolean isHasExternalId() {
                return hasExternalId;
        }

        public void setHasExternalId(boolean hasExternalId) {
                this.hasExternalId = hasExternalId;
        }

        public boolean isFiat() {
                return isFiat;
        }

        public void setFiat(boolean fiat) {
                isFiat = fiat;
        }

        public boolean isAvailable() {
                return isAvailable;
        }

        public void setAvailable(boolean available) {
                isAvailable = available;
        }

        @Override
        public String toString() {
                return "CurrencyItem{" +
                        "id='" + id + '\'' +
                        ", ticker='" + ticker + '\'' +
                        ", name='" + name + '\'' +
                        ", image='" + image + '\'' +
                        ", hasExternalId=" + hasExternalId +
                        ", isFiat=" + isFiat +
                        ", isAvailable=" + isAvailable +
                        '}';
        }
}
