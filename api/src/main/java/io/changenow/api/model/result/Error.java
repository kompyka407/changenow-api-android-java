package io.changenow.api.model.result;

class Error<T> implements Result<T> {

    private final Throwable throwable;

    Error(final Throwable throwable) {
        this.throwable = throwable;
    }

    @Override
    public boolean isOk() {
        return false;
    }

    @Override
    public T get() {
        return (T) throwable;
    }

    @Override
    public Throwable getError() {
        return throwable;
    }

    @Override
    public String toString() {
        return "Error{" +
                "throwable=" + throwable +
                '}';
    }

}
