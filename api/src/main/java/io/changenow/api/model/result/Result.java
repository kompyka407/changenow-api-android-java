package io.changenow.api.model.result;

public interface Result<T> {

    static <T> Result<T> ok(final T value) {
        if (value == null) throw new NullPointerException("The value of a Result cannot be null");
        return new Ok<>(value);
    }

    static <T, E extends Throwable> Result<T> error(final E throwable) {
        if (throwable == null) throw new NullPointerException("The error of a Result cannot be null");
        return new Error<>(throwable);
    }

    T get();

    boolean isOk();

    Throwable getError();

}
