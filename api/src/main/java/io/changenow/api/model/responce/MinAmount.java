package io.changenow.api.model.responce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by samosudovd on 07/06/2018.
 */

public class MinAmount {

        @Expose
        @SerializedName("minAmount")
        float minAmount;

        public float getMinAmount() {
                return minAmount;
        }

        public void setMinAmount(float minAmount) {
                this.minAmount = minAmount;
        }

        @Override
        public String toString() {
                return "MinAmount{" +
                        "minAmount=" + minAmount +
                        '}';
        }
}
